import React, { useEffect, useState } from "react";
import { Provider } from "react-redux";
import LoadGET from "./loadGet";
import LoadIP from "./loadIP";
import store from "./redux/store";

const App = () => {
  const [counter, setCounter] = useState(0);

  const [form, setForm] = useState({
    name: "",
    email: ""
  });

  useEffect(() => {
    setCounter(counter => counter + 1);
  }, []);

  const onChangeForm = ({ target }) => {
    setForm({
      ...form,
      [target.id]: target.value
    });
  };

  return (
    <>
      <Provider store={store}>
        <div className="App">My counter: {counter}</div>
        <LoadIP />
        <LoadGET />

        <div>
          <label htmlFor="name">Name</label>
          <input type="text" value={form.name} id="name" onChange={onChangeForm} />
        </div>

        <div>
          <label htmlFor="email">E-mail</label>
          <input type="text" value={form.email} id="email" onChange={onChangeForm} />
        </div>

        <pre>{JSON.stringify(form)}</pre>
      </Provider>
    </>
  );
};

export default App;
