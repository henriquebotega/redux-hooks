import React from "react";
import { connect } from "react-redux";
import { loadGet } from "./redux/action";

const LoadGET = props => {
  const carregar = () => {
    props.loadContent();
  };

  return (
    <div>
      Meu loadGET: {JSON.stringify(props.myGet)}
      <button onClick={() => carregar()}>Carregar</button>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    myGet: state.myReducerGet
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadContent: () => dispatch(loadGet())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoadGET);
