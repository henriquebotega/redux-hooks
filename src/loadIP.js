import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadIp } from "./redux/action";

const LoadIP = props => {
  const dispatch = useDispatch();
  const myIp = useSelector(state => state.myReducerIp);

  const carregar = () => {
    dispatch(loadIp());
  };

  return (
    <div>
      Meu loadIP: {JSON.stringify(myIp)}
      <button onClick={() => carregar()}>Carregar</button>
    </div>
  );
};

export default LoadIP;
