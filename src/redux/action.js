import axios from "axios";

/* IP */
export const loadIp = () => {
  return dispatch => {
    dispatch({ type: "GET_IP_REQUEST" });

    setTimeout(async () => {
      const res = await axios.get("http://httpbin.org/ip");
      dispatch({ type: "GET_IP_SUCCESS", payload: res.data });
    }, 1000);
  };
};

/* GET */
export const loadGet = () => {
  return dispatch => {
    dispatch({ type: "GET_GET_REQUEST" });

    setTimeout(async () => {
      const res = await axios.get("http://httpbin.org/get");
      dispatch({ type: "GET_GET_SUCCESS", payload: res.data.url });
    }, 1000);
  };
};
