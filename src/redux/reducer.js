/* IP */
const INITIAL_STATE_IP = { loading: false, data: null };

export const reducerIP = (state = INITIAL_STATE_IP, action) => {
  switch (action.type) {
    case "GET_IP_REQUEST":
      return {
        ...state,
        loading: true,
        data: null
      };

    case "GET_IP_SUCCESS":
      return {
        ...state,
        loading: false,
        data: action.payload
      };

    default:
      return state;
  }
};

/* GET */
const INITIAL_STATE_GET = { loading: false, data: null };

export const reducerGet = (state = INITIAL_STATE_GET, action) => {
  switch (action.type) {
    case "GET_GET_REQUEST":
      return {
        ...state,
        loading: true,
        data: null
      };

    case "GET_GET_SUCCESS":
      return {
        ...state,
        loading: false,
        data: action.payload
      };

    default:
      return state;
  }
};
