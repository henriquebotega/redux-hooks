import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { reducerGet, reducerIP } from "./reducer";

const store = createStore(
  combineReducers({
    myReducerIp: reducerIP,
    myReducerGet: reducerGet
  }),
  applyMiddleware(thunk)
);

export default store;
